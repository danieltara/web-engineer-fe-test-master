# Attest web engineer tech test

# Introduction

This is a tech test for the web engineer role at Attest. The focus is code (JS, HTML & CSS). 

The code runs on webpack, and supports TypeScript (but completing the test in JavaScript is ok).

Please do not use a JS framework such as Vue or React.

# Setting up / installation

> This assumes your local environment has node and yarn installed. If not, please refer to https://classic.yarnpkg.com/en/docs/install/

- Run `yarn` to install the dependencies.
- Run `yarn start` to run the dev server with hot reloading, then visit `http://localhost:8080`. 
- Make changes to `index.html`, the styles in `./src/style/index.scss`, and the TypeScript in `./src/index.ts`. 

# How to use JavaScript instead of TypeScript:

- If you want to only use JavaScript instead of TypeScript, just rename `index.ts` to `index.js`, then update `webpack.config.js` 'main' entry config to this:

```
    // ...
        entry: {
          main: './src/index.ts'
        },
    // ...
```

# The test

- We would like you to implement a carousel with 'testimonial quotes' (similar to what you can see near the bottom of https://www.askattest.com/).

- Please base it on the design on Figma: https://www.figma.com/file/BL8MCTa34YnrBVvmqHnhht/fe-test?node-id=0%3A1 

     - To inspect the elements, please download the Figma app on https://www.figma.com/downloads/  
     - We have also included a PDF output of the file in case Figma is not working. These can be found in the `designs` directory.

- There is currently one quote in the HTML (image + quotation), but please add interactivity so users can see all 4 quotes. Quote details are given at the bottom of this file.

- The `index.html` has the layout done in `<table>` - please replace this with a more modern approach.

- The left/right arrow buttons should trigger a change in the quote/image shown.

- Each quote should have a different background colour (hint: `.page-section--theme-green`)

- When viewing the final quote, pressing 'next' should take you back to the first image/quote. When viewing the first quote the 'previous' button should show the last image/quote.

- It should automatically cycle through to the next quote after 8 seconds.

- The test should take a max of 1-2 hrs to complete. Do not take any longer than this. 

- There are a few main parts to this test which we will pay attention to: 
    - HTML/CSS,
    - JavaScript implementation of the interactivity,
    - accessibility.

- We are not expecting any tests to be written. We are also not expecting the webpack config to be updated (but feel free to do so if it helps).

- We will be testing it only in Chrome on desktop. No need to add support for tablet/mobile size screens.

- If you have time, please add masking to the image. Do not worry about this if you do not have time. The SVG files for the mask shape & overlay can be found in `./src/images/mask.svg` and `./src/images/shape.svg`.

## Content to use
 - 4 Quotes:
 
 
 ```
      Quote: "It’s got that integrity within the data. It almost seemed a little too good to be true.",
      By: Grant Warnock, Head of Business Intelligence
      Image: https://placekitten.com/400/300
      Colour: green
     
      Quote: "It’s changed the way we think of ourselves and what we can do."
      By: Kate Szymanik, Senior Consultant
      Image: https://placekitten.com/500/300
      Colour: Blue
     
      Quote: "When I think about Attest, I think about it as the best of all worlds." 
      By: Ross Farquhar, Marketing Director
      Image: https://placekitten.com/450/305
      Colour: Yellow
     
      Quote: "It’s been irreplaceable, the kind of data we’ve had access to." 
      By: Tom Price, Head of Marketing & Innovation
      Image: https://placekitten.com/400/350
      Colour: Berry
```
