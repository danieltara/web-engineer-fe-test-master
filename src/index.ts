import './style/index.scss';

// Add your code here

class Main {
	timer: ReturnType<typeof setInterval>;

	constructor() {
		let prevBtn = document.getElementById( 'testimonials-nav__prev-button' ),
			nextBtn = document.getElementById( 'testimonials-nav__next-button' );

		this.timer = setInterval( function( cb: Function ) {
			cb();
		}, 8000, this.nextSlide );

		prevBtn.addEventListener( 'click', ( e:Event ) => {
			clearInterval( this.timer );

			this.prevSlide();

			this.timer = setInterval( function( cb: Function ) {
				cb();
			}, 8000, this.nextSlide );
		} );
		
		nextBtn.addEventListener( 'click', ( e:Event ) => {
			clearInterval( this.timer );

			this.nextSlide();

			this.timer = setInterval( function( cb: Function ) {
				cb();
			}, 8000, this.nextSlide );
		} );

		window.addEventListener( 'keyup', ( e:KeyboardEvent ) => {
			switch ( e.keyCode ) {
				case 37:
					clearInterval( this.timer );

					this.prevSlide();

					this.timer = setInterval( function( cb: Function ) {
						cb();
					}, 8000, this.nextSlide );

					break;

				case 39:
					clearInterval( this.timer );

					this.prevSlide();

					this.timer = setInterval( function( cb: Function ) {
						cb();
					}, 8000, this.nextSlide );

					break;
			}
		} );
	}

	prevSlide() {
		let currentSlide = ( document.querySelector( '.testimonial--state-active' ) as HTMLInputElement ),
			prevSlide    = currentSlide.previousSibling;

		while ( prevSlide != null && prevSlide.nodeName.toLowerCase() != 'article' ) {
			prevSlide = prevSlide.previousSibling;
		}

		if ( prevSlide == null ) {
			let slides = currentSlide.parentNode.children;

			// Get last slide
			for ( let i = slides.length - 1; i >= 0; i-- ) {
				if ( slides[ i ].nodeName.toLowerCase() == 'article' ) {
					prevSlide = slides[ i ] as HTMLInputElement;

					break;
				}
			}
		}

		currentSlide.classList.remove( 'testimonial--state-active' );
		currentSlide.removeAttribute( 'aria-current' );
		currentSlide.setAttribute( 'aria-hidden', 'true' );

		(<Element>prevSlide).classList.add( 'testimonial--state-active' );
		(<Element>prevSlide).removeAttribute( 'aria-hidden' );
		(<Element>prevSlide).setAttribute( 'aria-current', 'true' );
	}

	nextSlide() {
		let currentSlide = ( document.querySelector( '.testimonial--state-active' ) as HTMLInputElement ),
			nextSlide    = currentSlide.nextSibling;

		while ( nextSlide != null && nextSlide.nodeName.toLowerCase() != 'article' ) {
			nextSlide = nextSlide.nextSibling;
		}

		if ( nextSlide == null ) {
			let slides = currentSlide.parentNode.children;

			// Get first slide
			for ( let i = 0; i < slides.length; i++ ) {
				if ( slides[ i ].nodeName.toLowerCase() == 'article' ) {
					nextSlide = slides[ i ] as HTMLInputElement;

					break;
				}
			}
		}

		currentSlide.classList.remove( 'testimonial--state-active' );
		currentSlide.removeAttribute( 'aria-current' );
		currentSlide.setAttribute( 'aria-hidden', 'true' );

		(<Element>nextSlide).classList.add( 'testimonial--state-active' );
		(<Element>nextSlide).removeAttribute( 'aria-hidden' );
		(<Element>nextSlide).setAttribute( 'aria-current', 'true' );
	}
}

new Main();
