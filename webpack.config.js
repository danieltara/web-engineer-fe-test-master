const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const selectedPreprocessor = {
  fileRegexp: /\.(sass|scss|css)$/,
  loaderName: 'sass-loader',
};

module.exports = {
  entry: {
    // change to ./src/index.js if you want to only use Javascript:
    main: './src/index.ts',
  },
  devServer: {
    open: true,
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'js/[name].js',
  },
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      }, {
        test: selectedPreprocessor.fileRegexp,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              modules: false,
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: selectedPreprocessor.loaderName,
            options: {
              sourceMap: true,
            },
          },
        ],
	  }, {
	    test: /\.svg?$/,
	    include: path.resolve( __dirname, 'src/images' ),
	    use: [{
	    loader: 'file-loader',
	    options: {
	      name: '[name].[ext]',
	      outputPath: 'images/',
	      publicPath: '../images/',
	    },
	  }],
    },],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'style.css',
    }),
    new CopyWebpackPlugin({
      'patterns': [
        {
          from: './src/images',
          to: 'images',
        },
      ],
    }),
    new HtmlWebpackPlugin({
      inject: false,
      hash: false,
      template: './src/index.html',
      filename: 'index.html',
    }),
  ],
};
